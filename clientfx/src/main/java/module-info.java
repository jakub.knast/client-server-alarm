module clientfx {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.jk to javafx.fxml;
    exports com.jk;
}
